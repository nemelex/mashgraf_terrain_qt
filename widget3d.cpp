#include "widget3d.h"
#include <Qt>
#include <QKeyEvent>

#include <GL/glu.h>

#define _USE_MATH_DEFINES

#include <math.h>
#include <algorithm>
#include <iostream>

using std::min;
using std::max;
using std::cout;
using std::endl;
using std::for_each;
using std::unique_ptr;

Widget3D::Widget3D()
    :QGLWidget()
{
}

void Widget3D::initializeGL()
{
    qglClearColor(Qt::black);
    glEnable(GL_DEPTH_TEST);
    glFrontFace(GL_CW);
    //glEnable(GL_CULL_FACE);
}

void Widget3D::resizeGL(int nWidth, int nHeight)
{
    GLfloat nRange = 100.0f;
    if (nHeight == 0)
    {
        nHeight = 1;
    }
    if (nWidth == 0)
    {
        nWidth = 1;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    if (nWidth <= nHeight)
//    {
//        glOrtho(-nRange, nRange,
//                -nRange * nHeight / nWidth, nRange * nHeight / nWidth,
//                -nRange * 3, nRange * 3);
//    }
//    else
//    {
//        glOrtho(-nRange * nWidth/nHeight, nRange * nWidth/nHeight,
//                -nRange, nRange,
//                -nRange * 3, nRange * 3);
//    }


    glViewport(0, 0, nWidth, nHeight);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Widget3D::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // чистим буфер

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(50, 1.78, -200, 0);

    gluLookAt(-xPos, yPos, -zPos, xPos, yPos, zPos + 3000.0, 0, 1, 0);

//    glScalef(scaling, scaling, scaling);
//    glTranslatef(-xPos, -yPos, -zPos);
//    glRotatef(angleX, 1.0, 0.0, 0.0);
//    glRotatef(angleY, 0.0, 1.0, 0.0);
//    glRotatef(angleZ, 0.0, 0.0, 1.0);

    for_each(items.begin(), items.end(),
             [](unique_ptr<Drawable> &item){item -> Draw();});

    swapBuffers();
}

void Widget3D::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_Right:
            moveRight();
            break;
        case Qt::Key_Left:
            moveLeft();
            break;
        case Qt::Key_Up:
            moveForward();
            break;
        case Qt::Key_Down:
            moveBack();
            break;
        case Qt::Key_PageUp:
            moveUp();
            break;
        case Qt::Key_PageDown:
            moveDown();
            break;
        case Qt::Key_K:
            scalePlus();
            break;
        case Qt::Key_J:
            scaleMinus();
            break;
    }
    updateGL();
}

void Widget3D::rotateX(float angle)
{
    angleX += angle;
}

void Widget3D::rotateY(float angle)
{
    angleY += angle;
}

void Widget3D::addItem(Drawable *item)
{
    items.push_back(unique_ptr<Drawable>(item));
}

void Widget3D::moveForward()
{
    xPos += angleX;
    yPos += angleY;
    zPos += angleZ;
}

void Widget3D::moveBack()
{
    xPos -= angleX;
    yPos -= angleY;
    zPos -= angleZ;
}

void Widget3D::moveLeft()
{
    xPos -= moveStep;
}

void Widget3D::moveRight()
{
    xPos += moveStep;
}

void Widget3D::moveUp()
{
    yPos += moveStep;
}

void Widget3D::moveDown()
{
    yPos -= moveStep;
}

void Widget3D::mousePressEvent(QMouseEvent* event)
{
   ptrMousePosition = event -> pos();
}

void Widget3D::mouseMoveEvent(QMouseEvent *event)
{
    angleX += 180.0 * (GLfloat)(event -> y() - ptrMousePosition.y()) / height() / 5.0;
    angleY -= 180.0 * (GLfloat)(event -> x() - ptrMousePosition.x()) / width() / 5.0;
    updateGL();
}

void Widget3D::scalePlus()
{
    scaling *= 1.2;
}

void Widget3D::scaleMinus()
{
    scaling /= 1.2;
}

void rotate(float &vx, float &vy, float &vz, float ax, float ay, float az, float angle)
{
    double ca = cos(angle);
    double sa = sin(angle);
    double crossx = -vy * az + vz * ay;
    double crossy = -vz * ax + vx * az;
    double crossz = -vx * ay + vy * ax;
    double dot = ax * vx + ay * vy + az * vz;
    double rx = vx * ca + crossx * sa + dot * ax * (1 - ca);
    double ry = vy * ca + crossy * sa + dot * ay * (1 - ca);
    double rz = vz * ca + crossz * sa + dot * az * (1 - ca);
    vx = rx;
    vy = ry;
    vz = rz;
}

void Widget3D::rotateUp()
{
    rotate(xPos, yPos, zPos, Rx, Ry, Rz, 5);
}

void Widget3D::rotateDown()
{
    rotate(xPos, yPos, zPos, Rx, Ry, Rz, -5);
}

void Widget3D::rotateLeft()
{
    double Ux = Ry*angleZ - Rz*angleY;
    double Uy = Rz*angleX - Rx*angleZ;
    double Uz = Rx*angleY - Ry*angleX;

    rotate(Rx, Ry, Rz, Ux, Uy, Uz, 5);
    rotate(angleX, angleY, angleZ, Ux, Uy, Uz, 5);
}

void Widget3D::rotateRight()
{
    double Ux = Ry*angleZ - Rz*angleY;
    double Uy = Rz*angleX - Rx*angleZ;
    double Uz = Rx*angleY - Ry*angleX;

    rotate(Rx, Ry, Rz, Ux, Uy, Uz, -5);
    rotate(angleX, angleY, angleZ, Ux, Uy, Uz, -5);
}
