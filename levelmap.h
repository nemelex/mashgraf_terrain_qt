#ifndef LEVELMAP_H
#define LEVELMAP_H

#include "drawable.h"

#include <vector>
#include <tuple>

#define LOWEST -80
#define HIGHEST 80

class LevelMap : public Drawable
{
private:
    std::vector<std::vector<float>> data;
    void setColorFromHeight(float height, float min_height, float max_height);
public:
    LevelMap(const std::vector<std::vector<float>> &data);

    unsigned int getSize() noexcept;
    void setFromVector(const std::vector<std::vector<float>> &data);
    std::vector<std::vector<float>> getData();
    virtual ~LevelMap();
    virtual void Draw();
};

#endif // LEVELMAP_H
