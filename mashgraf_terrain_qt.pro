#-------------------------------------------------
#
# Project created by QtCreator 2014-01-24T04:36:11
#
#-------------------------------------------------

QT       += core gui
QT       += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mashgraf_terrain_qt
TEMPLATE = app


SOURCES += main.cpp \
    widget3d.cpp \
    levelmap.cpp \
    helpfunctions.cpp \
    drawable.cpp

HEADERS  += \
    widget3d.h \
    levelmap.h \
    errortype.h \
    diamondsquare.h \
    helpfunctions.h \
    drawable.h

LIBS += -lGLU

QMAKE_CXXFLAGS += -std=c++11 -Wall
