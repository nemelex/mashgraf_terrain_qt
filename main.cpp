#include "widget3d.h"
#include "levelmap.h"
#include "diamondsquare.h"

#include <QApplication>

#include <random>
#include <functional>

#define MAPSIZE 513
#define INIT_RAND 40
#define MULTIPLIER 0.67

int main(int argc, char *argv[])
{
    std::random_device rd;
    std::mt19937 rand_engine(rd());

    QApplication a(argc, argv);
    Widget3D w;
    w.setWindowTitle("Computer Graphics: Comission 3");
    w.addItem(new LevelMap{diamondSquareGen(MAPSIZE, LOWEST,
                                            HIGHEST, INIT_RAND,
                                            MULTIPLIER, rand_engine)});

    w.show();

    return a.exec();
}
