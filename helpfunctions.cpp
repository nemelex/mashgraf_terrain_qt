#include "helpfunctions.h"

using std::vector;

bool isPowerTwoPlusOne(unsigned int n)
{
    n = n - 1;
    while ((n > 1) && ((n % 2) == 0))
    {
        n /= 2;
    }
    return n == 1;
}

unsigned int indexOfSquareCenter(unsigned int square_side,
                                 unsigned int index_of_square)
{
    return (square_side - 1) / 2 + index_of_square * (square_side - 1);
}

void doSquare(std::vector<std::vector<float> > &map,
              unsigned int square_side, unsigned int square_center_x,
              unsigned int square_center_y, float shift)
{
    // corners are + or - square_side / 2
    float avg_corner = 0;
    vector<long> corner_index_shifts = {-int(square_side) / 2, int(square_side) / 2};
    for (int x_shift : corner_index_shifts)
    {
        for (int y_shift : corner_index_shifts)
        {
            avg_corner += map[int(square_center_x) + x_shift][int(square_center_y) + y_shift];
        }
    }
    map[square_center_x][square_center_y] = avg_corner / 4 + shift;
}

void doDiamond(std::vector<std::vector<float>> &map,
               unsigned int square_side, unsigned int square_center_x,
               unsigned int square_center_y, float shift)
{
    // corner indexes may be out of range, then we'll have to take it
    // from the other side
    float avg_corner = 0;
    vector<long> corner_index_shifts = {-int(square_side) / 2, int(square_side) / 2};
    for (int x_shift : corner_index_shifts)
    {
        int x_index = int(square_center_x) + x_shift;
        if (x_index >= int(map.size()))
        {
            x_index -= int(map.size());

        }
        else if (x_index < 0)
        {
            x_index += int(map.size());
        }
        avg_corner += map[x_index][square_center_y];

    }
    for (int y_shift : corner_index_shifts)
    {
        int y_index = square_center_y + y_shift;
        if (y_index >= int(map.size()))
        {
            y_index -= map.size();

        }
        else if (y_index < 0)
        {
            y_index += map.size();
        }
        avg_corner += map[square_center_x][y_index];
    }
    map[square_center_x][square_center_y] = avg_corner / 4 + shift;
}

