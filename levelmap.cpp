#include "levelmap.h"
#include "errortype.h"

#include "QGLWidget"

#include <functional>

using std::function;
using std::vector;

unsigned int LevelMap::getSize() noexcept
{
    return data.size();
}

LevelMap::~LevelMap()
{
}

void LevelMap::setFromVector(const std::vector<std::vector<float> > &data)
{
    unsigned int size = data.size();
    for (const auto &row : data)
    {
        if (row.size() != size)
        {
            throw ErrorType::BadDimensionNumber;
        }
    }
    this -> data = data;
}

LevelMap::LevelMap(const vector<vector<float>> &data)
{
    setFromVector(data);
}

vector<vector<float>> LevelMap::getData()
{
    return data;
}

void LevelMap::Draw()
{
    glPolygonMode(GL_FRONT, GL_FILL);
    glShadeModel(GL_SMOOTH);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    for (unsigned int i = 0; i < getSize() - 1; ++i)
    {
        glBegin(GL_TRIANGLE_STRIP);
        {
            for (unsigned int j = 0; j < getSize(); ++j)
            {
                setColorFromHeight(data[i][j], LOWEST, HIGHEST);
                glVertex3f(float(i) * 1.0, data[i][j], float(j) * 1.0);
                setColorFromHeight(data[i + 1][j], LOWEST, HIGHEST);
                glVertex3f(float(i +1) * 1.0, data[i + 1][j], float(j) * 1.0);
            }
        }
        glEnd();
    }
}

void LevelMap::setColorFromHeight(float height,
                               float min_height,
                               float max_height)
{
    // top 20% - snow
    // 50% - 80% - brown
    // 0% - 50% - grass

    function<float(float)> height_from_percents =
        [=](float percent){return min_height + (max_height - min_height) * percent;};
    float snow_min = height_from_percents(0.7);
    float brown_min = height_from_percents(0.5);
    if (height >= snow_min) // snow
    {
        float intensity = (height - min_height) / (max_height - min_height);
        glColor3f(intensity, intensity, intensity);
    }
    else if (height >= brown_min) // brown
    {
        float red_initial = 0.55;
        float green_initial = 0.27;
        float blue_initial = 0.07;
        float additional_color = (height - brown_min) / (snow_min - brown_min);
        additional_color /= 2;
        glColor3f(red_initial + additional_color,
                  green_initial + additional_color,
                  blue_initial + additional_color);
    }
    else // green
    {
        glColor3f(0.0, 0.15 + 1.5 * (height - min_height) / (max_height - min_height), 0.0);
    }
}
