#ifndef DIAMONDSQUARE_H
#define DIAMONDSQUARE_H

#include "levelmap.h"
#include "errortype.h"
#include "helpfunctions.h"

#include <random>
#include <vector>
#include <functional>

template<class RNG>
LevelMap diamondSquareGen(unsigned int size, float lower_treshold,
                          float upper_treshold, float max_initial_random,
                          float multiplier_for_random, RNG &generator)
{
    if (!isPowerTwoPlusOne(size))
    {
        throw ErrorType::BadDimensionNumber;
    }

    std::vector<std::vector<float>> map;
    map.resize(size);
    for (auto &row : map)
    {
        row.resize(size);
    }

    std::uniform_real_distribution<float> init_distr(lower_treshold, upper_treshold);

    // Calculate values in the corners
    for (const auto &i : std::vector<unsigned int>{0, size - 1})
    {
        for (const auto &j : std::vector<unsigned int>{0, size - 1})
        {
            map[i][j] = init_distr(generator);
        }
    }

    float random_shift_limit = max_initial_random;
    unsigned int square_side = size;
    for (; square_side > 2; square_side = square_side / 2 + 1)
    {
        std::uniform_real_distribution<float> rnd_shift_dist(-random_shift_limit,
                                                        random_shift_limit);
        std::function<float()> rnd_shift = [&rnd_shift_dist, &generator]
                                           (){return rnd_shift_dist(generator);};
        for (unsigned int i = 0; i < size / (square_side - 1); ++i)
        {
            for (unsigned int j = 0; j < size / (square_side - 1); ++j)
            {
                unsigned int square_center_x = indexOfSquareCenter(square_side, i);
                unsigned int square_center_y = indexOfSquareCenter(square_side, j);
                doSquare(map, square_side, square_center_x, square_center_y, rnd_shift());
                // indexes are the position of the center
                for (int shift : std::vector<int>{-int(square_side) / 2, int(square_side) / 2})
                {
                    doDiamond(map, square_side, square_center_x + shift,
                              square_center_y, rnd_shift());
                    doDiamond(map, square_side, square_center_x,
                              square_center_y + shift, rnd_shift());
                }
            }
        }
        random_shift_limit *= multiplier_for_random;
    }
    return {map};
}

#endif // DIAMONDSQUARE_H
