#ifndef HELPFUNCTIONS_H
#define HELPFUNCTIONS_H

#include <vector>

bool isPowerTwoPlusOne(unsigned int n);

unsigned int indexOfSquareCenter(unsigned int square_side,
                                 unsigned int index_of_square);

void doSquare(std::vector<std::vector<float>> &map,
              unsigned int square_side, unsigned int square_center_x,
              unsigned int square_center_y, float shift);

void doDiamond(std::vector<std::vector<float>> &map,
               unsigned int square_side, unsigned int square_center_x,
               unsigned int square_center_y, float shift);


#endif // HELPFUNCTIONS_H
