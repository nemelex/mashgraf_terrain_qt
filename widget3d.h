#ifndef WIDGET3D_H
#define WIDGET3D_H

#include "drawable.h"

#include <QWidget>
#include <QGLWidget>

#include <memory>
#include <list>

class Widget3D : public QGLWidget
{
private:
    Q_OBJECT
    float angleX = 0.0;
    float angleY = 0.0;
    float angleZ = 0.0;
    float rotatePerPress = 10;
    float moveStep = 5;

    float xPos = 0.0;
    float yPos = 5.0;
    float zPos = 0.0;

    float scaling = 1.0;

    float Rx = 0.0;
    float Ry = 1.0;
    float Rz = 0.0;

    QPoint ptrMousePosition;

    std::list<std::unique_ptr<Drawable>> items = {};

    void rotateX(float angle);
    void rotateY(float angle);
    void moveForward();
    void moveBack();
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
    void scalePlus();
    void scaleMinus();
    void rotateUp();
    void rotateDown();
    void rotateLeft();
    void rotateRight();
protected:
    virtual void initializeGL();
    virtual void resizeGL(int nWidth, int nHeight);
    virtual void paintGL();
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent* event);
public:
    Widget3D();
    void addItem(Drawable *item);
};

void rotate(float &vx, float &vy, float &vz, float ax, float ay, float az, float angle);

#endif // WIDGET3D_H
